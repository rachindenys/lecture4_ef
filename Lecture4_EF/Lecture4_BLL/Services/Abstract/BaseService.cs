﻿using AutoMapper;
using Lecture4_DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture4_BLL.Services.Abstract
{
    public abstract class BaseService<T> where T : class
    {
        private protected readonly AcademyDbContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(AcademyDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
