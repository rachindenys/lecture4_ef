﻿using AutoMapper;
using Lecture4_BLL.Exceptions;
using Lecture4_BLL.Services.Abstract;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.Project;
using Lecture4_DAL;
using Lecture4_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture4_BLL.Services
{
    public sealed class ProjectService : BaseService<Project>
    {
        public ProjectService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ProjectDTO> Create(ProjectDTO projectDTO)
        {
            var project = _mapper.Map<Project>(projectDTO);
            _context.Projects.Add(project);
            await _context.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(project);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var project = await _context.Projects.FindAsync(id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }
            _context.Projects.Remove(project);
            await _context.SaveChangesAsync();
        }

        public ICollection<ProjectDTO> Get()
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.Projects);
        }

        public ProjectDTO Get(int id)
        {
            var project = _context.Projects.Find(id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }
            return _mapper.Map<ProjectDTO>(project);
        }

        public async System.Threading.Tasks.Task Update(ProjectDTO projectDTO)
        {
            var project = await _context.Projects.FindAsync(projectDTO.Id);
            if (project == null)
            {
                throw new NotFoundException(nameof(Project), projectDTO.Id);
            }
            project.AuthorId = projectDTO.AuthorId;
            project.CreatedAt = projectDTO.CreatedAt;
            project.Deadline = projectDTO.Deadline;
            project.Description = projectDTO.Description;
            project.Name = projectDTO.Name;
            project.TeamId = projectDTO.TeamId;
            //project.Author = _mapper.Map<User>(projectDTO.Author);
            //project.Tasks = _mapper.Map<List<Lecture4_DAL.Entities.Task>>(projectDTO.Tasks);
            //project.Team = _mapper.Map<Team>(projectDTO.Team);
            _context.Projects.Update(project);
            await _context.SaveChangesAsync();
        }

    }
}
