﻿using AutoMapper;
using Lecture4_BLL.Exceptions;
using Lecture4_BLL.Services.Abstract;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.Task;
using Lecture4_DAL;
using Lecture4_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture4_BLL.Services
{
    public sealed class TaskService : BaseService<Lecture4_DAL.Entities.Task>
    {
        public TaskService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TaskDTO> Create(TaskDTO taskDTO)
        {
            var task = _mapper.Map<Lecture4_DAL.Entities.Task>(taskDTO);
            _context.Tasks.Add(task);
            await _context.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var task = await _context.Tasks.FindAsync(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture4_DAL.Entities.Task), id);
            }
            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        public ICollection<TaskDTO> Get()
        {
            return _mapper.Map<ICollection<TaskDTO>>(_context.Tasks);
        }

        public TaskDTO Get(int id)
        {
            var task = _context.Tasks.Find(id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture4_DAL.Entities.Task), id);
            }
            return _mapper.Map<TaskDTO>(task);
        }

        public async System.Threading.Tasks.Task Update(TaskDTO taskDTO)
        {
            var task = await _context.Tasks.FindAsync(taskDTO.Id);
            if (task == null)
            {
                throw new NotFoundException(nameof(Lecture4_DAL.Entities.Task), taskDTO.Id);
            }
            task.CreatedAt = taskDTO.CreatedAt;
            task.Description = taskDTO.Description;
            task.FinishedAt = taskDTO.FinishedAt;
            task.Name = taskDTO.Name;
            task.PerformerId = taskDTO.PerformerId;
            task.ProjectId = taskDTO.ProjectId;
            task.State = _mapper.Map<TaskState>(taskDTO.State);
            //task.Performer = _mapper.Map<User>(taskDTO.Performer);
            //task.Project = _mapper.Map<Project>(taskDTO.Project);
            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
        }
    }
}
