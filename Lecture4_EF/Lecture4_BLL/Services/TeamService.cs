﻿using AutoMapper;
using Lecture4_BLL.Exceptions;
using Lecture4_BLL.Services.Abstract;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.Team;
using Lecture4_DAL;
using Lecture4_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture4_BLL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TeamDTO> Create(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var team = await _context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public ICollection<TeamDTO> Get()
        {
            return _mapper.Map<ICollection<TeamDTO>>(_context.Teams);
        }

        public TeamDTO Get(int id)
        {
            var team = _context.Teams.Find(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            return _mapper.Map<TeamDTO>(team);
        }

        public async System.Threading.Tasks.Task Update(TeamDTO teamDTO)
        {
            var team = await _context.Teams.FindAsync(teamDTO.Id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), teamDTO.Id);
            }
            team.CreatedAt = teamDTO.CreatedAt;
            team.Name = teamDTO.Name;
            //team.Users = _mapper.Map<List<User>>(teamDTO.Users);
            _context.Teams.Update(team);
            await _context.SaveChangesAsync();
        }
    }
}
