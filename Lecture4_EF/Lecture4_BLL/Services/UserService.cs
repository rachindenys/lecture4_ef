﻿using AutoMapper;
using Lecture4_BLL.Exceptions;
using Lecture4_BLL.Services.Abstract;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.User;
using Lecture4_DAL;
using Lecture4_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture4_BLL.Services
{
    public sealed class UserService : BaseService<User>
    {
        public UserService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public ICollection<UserDTO> Get()
        {
            return _mapper.Map<ICollection<UserDTO>>(_context.Users);
        }

        public UserDTO Get(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }
            return _mapper.Map<UserDTO>(user);
        }

        public async System.Threading.Tasks.Task Update(UserDTO userDTO)
        {
            var user = await _context.Users.FindAsync(userDTO.Id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), userDTO.Id);
            }
            user.BirthDay = userDTO.BirthDay;
            user.Email = userDTO.Email;
            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.RegisteredAt = userDTO.RegisteredAt;
            //user.Tasks = _mapper.Map<List<Lecture4_DAL.Entities.Task>>(userDTO.Tasks);
            //user.Team = _mapper.Map<Team>(userDTO.Team);
            user.TeamId = userDTO.TeamId;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }
    }
}
