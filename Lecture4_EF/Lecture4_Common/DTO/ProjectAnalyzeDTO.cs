﻿using Lecture4_Common.DTO.Project;
using Lecture4_Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture4_Common.DTO
{
    public class ProjectAnalyzeDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}
