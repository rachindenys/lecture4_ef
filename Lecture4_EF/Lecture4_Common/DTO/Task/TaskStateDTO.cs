﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture4_Common.DTO.Task
{
    public enum TaskStateDTO
    {
        First = 0,
        Second = 1,
        Third = 2,
        Fourth = 3
    }
}
