﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Lecture4_Common.DTO.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        //public TeamDTO Team { get; set; }
        //public List<TaskDTO> Tasks { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
