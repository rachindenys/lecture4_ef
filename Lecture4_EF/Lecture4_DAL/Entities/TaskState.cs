﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture4_DAL.Entities
{
    public enum TaskState
    {
        Created = 0,
        Started = 1,
        Finished = 2,
        Cancelled = 3
    }
}
