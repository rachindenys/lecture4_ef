﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture4_BLL.Services;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.Project;

namespace Lecture4_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(
            ProjectService projectService
            )
        {
            _projectService = projectService;
        }

        [HttpPost]
        public async Task<ProjectDTO> Create([FromBody] ProjectDTO projectDTO)
        {
            return await _projectService.Create(projectDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _projectService.Delete(id);
        }
        [HttpGet]
        public ICollection<ProjectDTO> Get()
        {
            return _projectService.Get();

        }
        [HttpGet("{id}")]
        public ProjectDTO Get(int id)
        {
            return _projectService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] ProjectDTO projectDTO)
        {
            await _projectService.Update(projectDTO);
        }
    }
}
