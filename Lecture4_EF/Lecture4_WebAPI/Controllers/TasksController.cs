﻿using Lecture4_BLL.Services;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.Task;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture4_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(
            TaskService taskService
            )
        {
            _taskService = taskService;
        }

        [HttpPost]
        public async Task<TaskDTO> Create([FromBody] TaskDTO taskDTO)
        {
            return await _taskService.Create(taskDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _taskService.Delete(id);
        }
        [HttpGet]
        public ICollection<TaskDTO> Get()
        {
            return _taskService.Get();

        }
        [HttpGet("{id}")]
        public TaskDTO Get(int id)
        {
            return _taskService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] TaskDTO taskDTO)
        {
            await _taskService.Update(taskDTO);
        }
    }
}
