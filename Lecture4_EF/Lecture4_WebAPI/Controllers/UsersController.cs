﻿using Lecture4_BLL.Services;
using Lecture4_Common.DTO;
using Lecture4_Common.DTO.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lecture4_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(
            UserService userService
            )
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<UserDTO> Create([FromBody] UserDTO userDTO)
        {
            return await _userService.Create(userDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _userService.Delete(id);
        }
        [HttpGet]
        public ICollection<UserDTO> Get()
        {
            return _userService.Get();

        }
        [HttpGet("{id}")]
        public UserDTO Get(int id)
        {
            return _userService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] UserDTO userDTO)
        {
            await _userService.Update(userDTO);
        }
    }
}
